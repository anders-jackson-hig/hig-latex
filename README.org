#+title: Råd inför uppsatsskrivande i \LaTeX{}
#+author: Anders Jackson
#+date: 2023-02-09
#+lang: se

* Introduktion

Här finns några filer att titta på. Det är meningen att det skall
kunna bli en sorts mall på hur man enkelt hanterar \LaTeX. Kopiera dem
med fördel ner i en egen katalog och experimentera med dem.

Klona och gör ändringar, skickar sedan gärna förändringar uppströms.

Dokumentet =mall.pdf= skapas genom att köra kommandot =make all= i
katalogen.

#+caption: Hur man skapar =PDF= från \LaTeX-filer.
#+name: src:create_all
#+begin_src shell
  make all
#+end_src

** Installation av programvara i MS Windows

Om man använder MS Windows 10 eller 11, så kan man installera 
[[https://apps.microsoft.com/store/detail/ubuntu/9PDXGNCFSCZV][Ubuntu]]
eller 
[[https://apps.microsoft.com/store/detail/debian/9MSVKQC78PK6][Debian]]
direkt från Microsoft Store.

Sedan är det enkelt att installera \LaTeX{}, om man i Linux:en skriver
följande instruktioner i listningen [[src:install_latex]].

#+caption: Hur man installerar \LaTeX{} i Debian och Ubuntu.
#+name: src:install_latex
#+begin_src shell
  sudo apt update; sudo apt upgrade
  sudo apt install build-essential texlive-full
#+end_src

* Beskrivning av filer

Här är en beskrivning av relevanta filer i detta projekt.

+ =mall.tex= :: Detta är källkoden till dokumentet.
+ =Makefile= :: Detta är en konfigureringsfil till =make= för att man
  lättare skall kunna skapa =Postscript= och =PDF=-filer från
  =mall.text= utan att behöva komma ihåg alla kommandon. Prova att
  exekvera kommandot =make= i katalogen med filerna för att få
  instruktioner.
+ =mall.pdf= :: =PDF=-variant av dokumentet.
+ =bibfile.bib= :: Samling av bibliografier till dokumentet.
+ =mallen.tex= :: Exempeldokument till =mall.tex=.
+ =test.bib= :: Exempelsamling av bibliografier till =mall.tex=.
+ =test1.eps= :: Bild ett i dokumentet.
+ =test2.eps= :: Bild två i dokumentet.
+ =rapportmall/= :: Filen med =xjobb.sty= för att genererar försättsblad
  till examensjobb vid HiG som /Marcus Rejås/ skapade för länge sedan
  är flyttad till eget förråd.
  Behöver uppdateras.

-----
* COMMENT Lägg till stöd för att köra =shell=-skript.

#+begin_src elisp
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)))
#+end_src
