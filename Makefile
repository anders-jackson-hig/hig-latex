########################################################################
#
# Makefile for example document in LaTeX
#
# It make some dependencies between latex source and 
# some destination formats like PostScript and PDF.
#
# Date       Who               Why
# ============================================================
# 2001-03-08 Anders Jackson    Automatic generation of reports
#                              in PS och PDF-formats
#
#

##########
# Lets define some things
# Notice that pdflatex can generate PDF documents directly, without passing DVIPS and PS2PDF.
LATEX=latex
BIBTEX=bibtex
DVIPS=dvips
DVIPSFLAGS=-N0
PSTOPDF=ps2pdf
PSTOPDFFLAGS=

##########
# Some teaching to do
# Teach Make to do .dvi from .tex
%.dvi : %.tex
	if [ ! -f `basename $< .tex`.aux ]; then $(LATEX) $(LATEXFLAGS) $<; fi
	if [ ! -f `basename $< .tex`.bbl ]; then $(BIBTEX) $(BIBTEXFLAGS) `basename $< .tex `; fi
	$(LATEX) $(LATEXFLAGS) $<
	$(LATEX) $(LATEXFLAGS) $<

# Teach Make to do .ps from .dvi
%.ps : %.dvi
	$(DVIPS) $(DVIPSFLAGS) -o $@ $<

# Teach Make to do .pdf from .ps
%.pdf : %.ps
	$(PSTOPDF) $< $@

##########
# All source files
SOURCE=    mall.tex
OUTPUTPS=  $(SOURCE:.tex=.ps)
OUTPUTPDF= $(OUTPUTPS:.ps=.pdf)

ALLFILES=  Makefile $(SOURCE)

##########
# Lets make some relations
.PHONEY: info all ps pdf clean distclean
info :
	@echo "Run make with one of these arguments"
	@echo ""
	@echo "all       -- make all"
	@echo "ps        -- make all PostScript"
	@echo "pdf       -- make all PDF-files"
	@echo ""
	@echo "clean     -- clean up"
	@echo "distclean -- realy clean up"
	@echo "install   -- install pages"

all: ps pdf

distclean:: clean
	-$(RM) $(SOURCE:.tex=.dvi)
	-$(RM) $(SOURCE:.tex=.toc)
	-$(RM) $(SOURCE:.tex=.lot)
	-$(RM) $(SOURCE:.tex=.lof)
	-$(RM) $(SOURCE:.tex=.bbl)
	-$(RM) $(SOURCE:.tex=.blg)
clean::
	-$(RM) *~
	-$(RM) $(SOURCE:.tex=.log)
	-$(RM) $(SOURCE:.tex=.aux)

ps : $(OUTPUTPS)
distclean::
	-$(RM) $(OUTPUTPS)

pdf : $(OUTPUTPDF)
distclean::
	-$(RM) $(OUTPUTPDF)

#
# EOF
#
